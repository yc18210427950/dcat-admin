<!-- 此视图文件位置 resources/views/gushi/web_list.blade.php -->
<!DOCTYPE html>
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>古诗词</title>
</head>
<body>

	<div style="width: 300px;">
		
		@foreach ($list as $vo)
		    
		    <p>
				<a href="{{url('gushici/detail',['unid'=>$vo->unid])}}" target="_blank">
					{{ $vo->title }}
				</a>
			</p>
		@endforeach
		
	</div>


</body>
</html>
