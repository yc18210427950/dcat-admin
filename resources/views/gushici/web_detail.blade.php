<!-- 此视图文件位置 resources/views/gushi/web_show.blade.php -->
<!DOCTYPE html>
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>古诗词</title>
</head>
<body>

	<div style="width: 300px;">
		
		<img src="{{ $show_img_url }}" alt="">
		
		<div>
			<center>
				{{ $data->title }}
			</center>
		</div>
		
		<div>
			<center>
				<pre>
					{!! htmlspecialchars_decode($data->content) !!}
				</pre>
			</center>
		</div>
		
		<div>
			<center>
				<pre>
					<span style="width: 300px;">
						{!! htmlspecialchars_decode($data->notes) !!}
					</span>
					
				</pre>
			</center>
		</div>
		
		
		
	</div>


</body>
</html>
