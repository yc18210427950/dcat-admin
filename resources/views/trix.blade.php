<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <title>Trix 富文本编辑器演示页面</title>
        @trixassets
    </head>
    <body>
        @trix(\App\Post::class, 'body')
    </body>
</html>