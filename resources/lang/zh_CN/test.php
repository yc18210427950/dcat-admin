<?php 
return [
    'labels' => [
        'Test' => '测试',
        'test' => '测试',
    ],
    'fields' => [
        'title' => '标题',
        'intro' => '简介',
        'content' => '内容',
    ],
    'options' => [
    ],
];
