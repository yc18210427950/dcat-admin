<?php 
return [
    'labels' => [
        'Gushici' => '古诗词',
        'gushici' => '古诗词',
    ],
    'fields' => [
        'title' => '标题',
        'auther' => '作者',
        'content' => '内容',
        'notes' => '注释',
    ],
    'options' => [
    ],
];
