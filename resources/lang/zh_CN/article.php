<?php 
return [
    'labels' => [
        'Article' => '文章',
        'article' => '文章',
    ],
    'fields' => [
        'title' => '标题',
        'intro' => '简介',
        'content' => '内容',
        'user_id' => '用户ID',
        'date' => '日期',
    ],
    'options' => [
    ],
];
