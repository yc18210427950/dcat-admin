<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use wycto\helper\HelperCommon;
use wycto\helper\HelperSpell;

use Pinyin; // Facade class, NOT Overtrue\Pinyin\Pinyin

// use Emadadly\LaravelUuid\Uuids;

use Jenssegers\Agent\Agent;

use Ixudra\Curl\Facades\Curl;

class HelperController extends Controller
{
    //
	/**
	 * 助手函数
	 * @param {Object} Request $request
	 */
	public function web_index (Request $request)
	{
	
		// \Debugbar::enable();
		// // \Debugbar::disable();
		// // Debugbar::info($object);
		// \Debugbar::error('Error!');
		// // \Debugbar::warning('Watch out…');
		// // \Debugbar::addMessage('Another message', 'mylabel');	
		
		// die;
		
		$res = HelperSpell::getPinYin('测试');
		
		var_dump($res);
		
		dump($res);
		
		$ip = HelperCommon::getip();
		
		dump($ip);
		
		
		// $pinyin = app('pinyin');
		// echo $pinyin->sentence('带着希望去旅行，比到达终点更美好');
		// // dài zhe xī wàng qù lǔ xíng, bǐ dào dá zhōng diǎn gèng měi hǎo
		
		
		$pinyin = Pinyin::sentence('带着希望去旅行，比到达终点更美好');
		// dài zhe xī wàng qù lǔ xíng, bǐ dào dá zhōng diǎn gèng měi hǎo
		
		dump($pinyin);
		
		// $example = Uuid::uuid1('uuid');
		
		// dump($example);
		
		\Debugbar::enable();
		// \Debugbar::disable();
		// Debugbar::info($object);
		\Debugbar::error('Error!');
		// \Debugbar::warning('Watch out…');
		// \Debugbar::addMessage('Another message', 'mylabel');
		
		
		$agent = new Agent();
		
		dump($agent);
		
		
		// Send a GET request to: http://www.foo.com/bar
		$response = Curl::to('https://www.baidu.com')
			->get();
			
		dump($response);
		
		
		\Former::framework('TwitterBootstrap3');
		
		\Former::horizontal_open()
		  ->id('MyForm')
		  ->rules(['name' => 'required'])
		  ->method('GET');
		
		  \Former::xlarge_text('name') # Bootstrap sizing
		    ->class('myclass') # arbitrary attribute support
		    ->label('Full name')
		    ->value('Joseph')
		    ->required() # HTML5 validation
		    ->help('Please enter your full name');
		
		  \Former::textarea('comments')
		    ->rows(10)
		    ->columns(20)
		    ->autofocus();
		
		  \Former::actions()
		    ->large_primary_submit('Submit') # Combine Bootstrap directives like "lg and btn-primary"
		    ->large_inverse_reset('Reset');
		
		\Former::close();
	
	// 	// Send a GET request to: http://www.foo.com/bar?foz=baz
	// 	$response = Curl::to('http://www.foo.com/bar')
	// 		->withData( array( 'foz' => 'baz' ) )
	// 		->get();
	
	// 	// Send a GET request to: http://www.foo.com/bar?foz=baz using JSON
	// 	$response = Curl::to('http://www.foo.com/bar')
	// 		->withData( array( 'foz' => 'baz' ) )
	// 		->asJson()
	// 		->get();
		
		die;
	}
}
