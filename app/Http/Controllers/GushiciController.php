<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

// use App\Http\Models\AdminUser;
use App\Http\Resources\Gushici as GushiciResource;

class GushiciController extends Controller
{
    
	//
	public function web_show (Request $request)
	{
	
		$data = DB::table('gushici')
				// ->whereNotNull('deleted_at')
				->whereNull('deleted_at')
				->orderBy(DB::raw('RAND()')) 
				->limit('1')
				->get();
		// var_dump($data);die;
		return view('gushici.web_show', ['data'=>$data[0]]);
	}
	
	/**
	 * 古诗词列表
	 * @param {Object} Request $request
	 */
	public function web_list (Request $request)
	{
	
		$list = DB::table('gushici')
				// ->whereNotNull('deleted_at')
				->whereNull('deleted_at')
				->orderBy('unid','desc') 
				->get();
		// var_dump($data);die;
		return view('gushici.web_list', ['list'=>$list]);
	}
	
	/**
	 * 古诗词详情
	 * @param {Object} Request $request
	 */
	public function web_detail (Request $request, int $unid = 0)
	{
		$data = DB::table('gushici')
				->whereNull('deleted_at')
				->where('unid',$unid)
				->get();
		// var_dump($data);die;
		
		// $transform = new \Text2pic\Transform('by text2pic');
		// $result = $transform->generate("hello world");
		// print_r($result);
		
		$base_path = base_path();
		
		$by = 'YANG';
		$uploadsPath = $base_path.'/public/text2pic';
		$uploadsUrl = '/text2pic';
		$fontPath = '';
		
		$transform=new \Text2pic\Transform($by,$uploadsPath,$uploadsUrl,$fontPath);
		$result = $transform->generate('杨驰');
		
		$img_url = $result['data']['url'];
		
		$show_img_url = url($img_url);
		
		return view('gushici.web_detail', [
					'data'=>$data[0],
					'show_img_url' => $show_img_url
				]);
	}

	
}
