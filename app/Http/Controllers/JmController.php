<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Crypt;

use Illuminate\Contracts\Encryption\DecryptException;

class JmController extends Controller
{
    //
	
	/**
	 * Store a secret message for the user.
	 *
	 * @param  Request  $request
	 * @return Response
	 */
	public function encrypt(Request $request)
	{
		echo Crypt::encryptString('123456');
	}
	
	/**
	 * @param {Object} Request $request
	 */
	public function decrypt(Request $request)
	{
		
		// $pdf = App::make('dompdf.wrapper');
		// $pdf->loadHTML('<h1>Test</h1>');
		// return $pdf->stream();
		
		
		// $pdf = PDF::loadView('pdf.invoice', $data);
		// return $pdf->download('invoice.pdf');
		
		// die;
		
		$json = array();
		
		$json['code'] = 1;
		$json['msg'] = '';
		$json['data'] = array(
							array(
								'title' => '测试',
								'content' => '测试'
							),
							array(
								'title' => '测试',
								'content' => '测试'
							),
							array(
								'title' => '测试',
								'content' => '测试'
							),
							array(
								'title' => '测试',
								'content' => '测试'
							),
							array(
								'title' => '测试',
								'content' => '测试'
							),
							array(
								'title' => '测试',
								'content' => '测试'
							),
							array(
								'title' => '测试',
								'content' => '测试'
							),
							array(
								'title' => '测试',
								'content' => '测试'
							),
						);
		$json = json_encode($json);
		$str = $json;
		
		$encryptedValue = Crypt::encryptString($str);
		
		$decrypted = Crypt::decryptString($encryptedValue);
		
		echo $encryptedValue;
		echo "<hr/>";
		echo $decrypted;
		
	}
	
	
}
