<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use UrlSigner;
use Alert;

class TestDemoController extends Controller
{
    //
	/**
	 * @param {Object} Request $request
	 */
	public function web_index(Request $request)
	{
	
		$url = url('gushici/list');
		
		echo UrlSigner::sign($url);
		
		// Alert::message('Message', 'Optional Title');
		
		die;
		
		return view('test_demo.web_index');
		
		die;
		
		//the generated URL will be valid for 5 days.
		UrlSigner::sign('https://myapp.com/protected-route', 5);
		
		//This URL will be valid up until 2 hours from the moment it was generated.
		UrlSigner::sign('https://myapp.com/protected-route', Carbon\Carbon::now()->addHours(2) );
		
		
		UrlSigner::validate('https://app.com/protected-route?expires=xxxxxx&signature=xxxxxx');
		
		
		// Route::get('protected-route', ['middleware' => 'signedurl', function () {
		//     return 'Hello secret world!';
		// }]);
		
	}
}
