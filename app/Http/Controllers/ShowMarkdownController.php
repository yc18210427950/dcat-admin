<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use GrahamCampbell\Markdown\Facades\Markdown;

class ShowMarkdownController extends Controller
{
    //
	/**
	 * @param {Object} Request $request
	 */
	public function web_index(Request $request)
	{
		echo Markdown::convertToHtml('foo'); // <p>foo</p>
	}
}
