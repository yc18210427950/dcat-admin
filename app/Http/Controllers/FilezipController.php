<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use ZanySoft\Zip\ZipManager;
use Zip;

class FilezipController extends Controller
{
    //
	/**
	 * @param {Object} Request $request
	 */
	public function web_index(Request $request)
	{
		// init manager
		$manager = new ZipManager();
		
		// // register existing zips
		// $manager->addZip( Zip::open('/path/to/my/file1.zip') )
		//         ->addZip( Zip::open('/path/to/my/file2.zip') );
		
		// // register a new zip
		// $manager->addZip( Zip::create('/path/to/my/file3.zip') );
		
		
		// separate content in folders
		$extract = $manager->extract('/Users/yangchi/yangchi/file_zip', true);
		
		// // use a single folder
		// $extract = $manager->extract('/path/to/uncompressed/files', false);
		
		// // extract single file
		// $extract = $manager->extract('/path/to/uncompressed/files', false, 'file');
		
		// // extract multiple files
		// $extract = $manager->extract('/path/to/uncompressed/files', false, array('file1','file2'));
		
		
	}
}
