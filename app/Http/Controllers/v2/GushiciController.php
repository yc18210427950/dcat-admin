<?php

namespace App\Http\Controllers\v2;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

// use App\Http\Models\AdminUser;
use App\Http\Resources\Gushici as GushiciResource;

class GushiciController extends Controller
{
	//
	public function api_show_one (Request $request)
	{

		$data = DB::table('gushici')
				// ->whereNotNull('deleted_at')
				->whereNull('deleted_at')
				->orderBy(DB::raw('RAND()')) 
				->limit('1')
				->get()
				->map(function ($value) {return (array)$value;})
				->toArray();
		// return Response::json($data[0]);
		// var_dump($data[0]);die;
		// var_dump(nulltostr($data[0]));die;
		
		
		$data = nulltostr($data[0]);
		
		$create_time = strtotime($data['created_at']);
		
		$data['date'] = date('Y-m-d',strtotime($data['created_at']));
		$data['date_time'] = date('m-d H:i',$create_time);
		
		$data['time_tran'] = time_tran($data['created_at']);
		
		
		$data_res = array();
		$data_res['code'] = 1;
		$data_res['msg'] = '';
		
		$data_res['data'] = $data;
		
		return response()->json($data_res)->setEncodingOptions(JSON_UNESCAPED_UNICODE);
		
	}
	
}
