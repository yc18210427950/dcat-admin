<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Jenssegers\Date\Date;

use Pinyin; // Facade class, NOT Overtrue\Pinyin\Pinyin


class ShowDateController extends Controller
{
    //
	/**
	 * @param {Object} Request $request
	 */
	public function web_index(Request $request)
	{
		
		var_dump(Pinyin::convert('带着希望去旅行'));
		// ["dai", "zhe", "xi", "wang", "qu", "lv", "xing"]
		
		echo Pinyin::sentence('带着希望去旅行，比到达终点更美好');
		// dài zhe xī wàng qù lǔ xíng, bǐ dào dá zhōng diǎn gèng měi hǎo
		
		
		
		$pinyin = app('pinyin');
		echo $pinyin->sentence('带着希望去旅行，比到达终点更美好');
		// dài zhe xī wàng qù lǔ xíng, bǐ dào dá zhōng diǎn gèng měi hǎo
		
		echo "<hr/>";
	
		$date = Date::now();
		
		Date::setLocale('nl');
		
		echo Date::now()->format('l j F Y H:i:s'); // zondag 28 april 2013 21:58:16
		
		echo "<hr/>";
		
		echo Date::parse('-1 day')->diffForHumans(); // 1 dag geleden
		
		echo "<hr/>";
		
		echo $date->timespan(); // 3 months, 1 week, 1 day, 3 hours, 20 minutes
		
		
		
		// Using Eloquent
		// return Datatables::eloquent(User::query())->make(true);
	}
}
