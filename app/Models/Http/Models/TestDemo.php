<?php

namespace App\Models\Http\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TestDemo extends Model
{
    use HasFactory;
}
