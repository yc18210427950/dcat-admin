<?php

namespace App\Models\Http\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ShowMarkdown extends Model
{
    use HasFactory;
}
