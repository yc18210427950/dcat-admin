<?php

	date_default_timezone_set('PRC');//其中PRC为“中华人民共和国”
	
	
	/**
	 * 将数组中的null转为字符串''
	 * @param $arr
	 */
	function nulltostr($arr)
	{
	
	    foreach ($arr as $k=>$v){
	        if(is_null($v)) {
	            $arr [$k] = '';
	        }
	        if(is_array($v)) {
	            $arr [$k] = nulltostr($v);
	        }
	    }
	    return $arr;
	}
	
	/**
	 * 几秒前，几分钟前，几小时前，几天前
	 * @param {Object} $the_time
	 */
	function time_tran($the_time){
		// $now_time = date("Y-m-d H:i:s",time()+8*60*60); 
		$now_time = date("Y-m-d H:i:s",time()); 
		$now_time = strtotime($now_time);
		$show_time = strtotime($the_time);
		$dur = $now_time - $show_time;
		if($dur < 0){
			return $the_time; 
		}else{
			if($dur < 60){
				return $dur.'秒前'; 
			}else{
				if($dur < 3600){
					return floor($dur/60).'分钟前'; 
				}else{
					if($dur < 86400){
						return floor($dur/3600).'小时前'; 
					}else{
						if($dur < 259200){//3天内
							return floor($dur/86400).'天前';
						}else{
							return $the_time; 
						}
					}
				}
			}
		}
	}

?>