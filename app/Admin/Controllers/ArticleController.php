<?php

namespace App\Admin\Controllers;

use App\Admin\Repositories\Article;
use Dcat\Admin\Form;
use Dcat\Admin\Grid;
use Dcat\Admin\Show;

use Dcat\Admin\Admin;

use Dcat\Admin\Http\Controllers\AdminController;

class ArticleController extends AdminController
{
    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Grid::make(new Article(), function (Grid $grid) {
            
            // 添加默认查询条件
            $user_id = Admin::user()->getKey();
            $grid->model()->where('user_id', '=', $user_id);
            
            $grid->column('article_id')->sortable();
            $grid->column('title');
            $grid->column('intro');
            // $grid->column('content');
            // $grid->column('user_id')->value(Admin::user()->getKey());
            // $grid->date('date')->format('YYYY-MM-DD');
            $grid->column('created_at');
            $grid->column('updated_at')->sortable();

            $grid->filter(function (Grid\Filter $filter) {
                $filter->equal('article_id');

            });
        });
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     *
     * @return Show
     */
    protected function detail($id)
    {
        return Show::make($id, new Article(), function (Show $show) {
            $show->field('article_id');
            $show->field('title');
            // $show->field('intro');
            $show->intro('简介');
            // $show->field('content');
            // $show->html('内容');

            $show->content('内容')->unescape()->as(function ($content) {
                return $content;
            });

            // $show->field('user_id')->value(Admin::user()->getKey());
            // $show->date('日期')->format('YYYY-MM-DD');
            $show->date('日期');
            $show->field('created_at');
            $show->field('updated_at');
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Form::make(new Article(), function (Form $form) {
            $form->display('article_id');
            $form->text('title');
            $form->textarea('intro');
            $form->editor('content');
            $form->hidden('user_id')->value(Admin::user()->getKey());
            $form->date('date')->format('YYYY-MM-DD');

            $form->display('created_at');
            $form->display('updated_at');
        });
    }
}
