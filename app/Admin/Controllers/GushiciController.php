<?php

namespace App\Admin\Controllers;

use App\Admin\Repositories\Gushici;
use Dcat\Admin\Form;
use Dcat\Admin\Grid;
use Dcat\Admin\Show;
use Dcat\Admin\Http\Controllers\AdminController;

class GushiciController extends AdminController
{
    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Grid::make(new Gushici(), function (Grid $grid) {
            $grid->column('unid')->sortable();
            $grid->column('title');
            $grid->column('auther');
            // $grid->column('content');
            // $grid->column('notes');
            $grid->column('created_at');
            $grid->column('updated_at')->sortable();
        
            $grid->filter(function (Grid\Filter $filter) {
                $filter->equal('unid');
        
            });
        });
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     *
     * @return Show
     */
    protected function detail($id)
    {
        return Show::make($id, new Gushici(), function (Show $show) {
            $show->field('unid');
            $show->field('title');
            $show->field('auther');
            $show->field('content');
            $show->field('notes');
            $show->field('created_at');
            $show->field('updated_at');
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Form::make(new Gushici(), function (Form $form) {
            $form->display('unid');
            $form->text('title');
            $form->text('auther');
			
            $form->editor('content');
            $form->editor('notes');
        
            $form->display('created_at');
            $form->display('updated_at');
        });
    }
}
