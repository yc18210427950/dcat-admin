<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


// use App\Http\Controllers\GushiciController;
// Route::get('/', [GushiciController::class, 'api_show_one']);


$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function($api) {
    $api->get('version', function() {
        return response('this is version v1');
    });
});

// $api->version('v2', function($api) {
//     $api->get('version', function() {
//         return response('this is version v2');
//     });
// });

use App\Http\Controllers\v1\GushiciController as GushiciController_v1;
Route::prefix('v1')->group(function() {

    // Route::get('list','ListController@index');
	
	Route::get('/', [GushiciController_v1::class, 'api_show_one']);
	Route::get('/gushici_list', [GushiciController_v1::class, 'api_list']);

});

use App\Http\Controllers\v2\GushiciController as GushiciController_v2;
Route::prefix('v2')->group(function() { 

    // Route::get('method','v2\\MethodController@index');
	
	Route::get('/', [GushiciController_v2::class, 'api_show_one']);

});




