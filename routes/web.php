<?php

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return Inertia::render('Welcome', [
//         'canLogin' => Route::has('login'),
//         'canRegister' => Route::has('register'),
//         'laravelVersion' => Application::VERSION,
//         'phpVersion' => PHP_VERSION,
//     ]);
// });

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return Inertia::render('Dashboard');
})->name('dashboard');

 Route::group(['prefix' => 'laravel-filemanager', 'middleware' => ['web']], function () {
     \UniSharp\LaravelFilemanager\Lfm::routes();
 });

use App\Http\Controllers\GushiciController;
Route::get('/', [GushiciController::class, 'web_show']);
Route::get('/gushici/list', [GushiciController::class, 'web_list']);
Route::get('/gushici/detail/{unid}', [GushiciController::class, 'web_detail']);

use App\Http\Controllers\JmController;
Route::get('/jm/encrypt', [JmController::class, 'encrypt']);
Route::get('/jm/decrypt', [JmController::class, 'decrypt']);


use App\Http\Controllers\HelperController;
Route::get('/helper', [HelperController::class, 'web_index']);

use App\Http\Controllers\UsersController;
Route::get('users/export/', [UsersController::class, 'export']);

use App\Http\Controllers\ShowDateController;
Route::get('show_date', [ShowDateController::class, 'web_index']);

use App\Http\Controllers\ShowMarkdownController;
Route::get('show_markdown', [ShowMarkdownController::class, 'web_index']);

use App\Http\Controllers\FilezipController;
Route::get('file_zip', [FilezipController::class, 'web_index']);


use App\Http\Controllers\TestDemoController;
Route::get('test_demo', [TestDemoController::class, 'web_index']);


Route::get('trix', function () {
    return view('trix');
});